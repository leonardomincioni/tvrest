package it.pegaso.tvrest.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="abbonamento")
public class Abbonamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="descrizione")
	private String descrizione;
	@OneToMany(mappedBy = "abbonamento", cascade = CascadeType.ALL)
	private List<Canale> canali;
	@OneToMany(mappedBy = "abbonamento", cascade = CascadeType.ALL)
	private List<Cliente> clienti;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public List<Canale> getCanali() {
		return canali;
	}
	public void setCanali(List<Canale> canali) {
		this.canali = canali;
	}
	public List<Cliente> getClienti() {
		return clienti;
	}
	public void setClienti(List<Cliente> clienti) {
		this.clienti = clienti;
	}
	
	
	
	
	
}
