package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvrest.models.Canale;

public interface CanaleRepository extends JpaRepositoryImplementation<Canale, Integer> {

}
