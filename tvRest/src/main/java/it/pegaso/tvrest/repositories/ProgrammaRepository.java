package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvrest.models.Programma;

public interface ProgrammaRepository extends JpaRepositoryImplementation<Programma, Integer> {

}
