package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvrest.models.Abbonamento;

public interface AbbonamentoRepository extends JpaRepositoryImplementation<Abbonamento, Integer> {

}
