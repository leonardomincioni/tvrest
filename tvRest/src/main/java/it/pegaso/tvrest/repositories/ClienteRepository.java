package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvrest.models.Cliente;

public interface ClienteRepository extends JpaRepositoryImplementation<Cliente, Integer>{

}
